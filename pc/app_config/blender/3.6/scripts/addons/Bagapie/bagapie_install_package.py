import bpy
import zipfile
import json
import glob
import os
from os import walk
import addon_utils
from bpy.types import Operator
import shutil
from bpy.props import StringProperty, BoolProperty
from bpy_extras.io_utils import ImportHelper
import ntpath


baga_libraries = [
    'Bagapieassets_database_free.blend',
    'Bagapieassets_database_V1.blend',
    'Bagapieassets_database_V2.blend',
    'Bagapieassets_database_V3.blend',
    'Bagapieassets_database_V4.blend',
    'Bagapieassets_database_V5.blend',
    'Bagapieassets_database_V6.blend',
    'Bagapieassets_database_V7.blend',
    'Bagapieassets_database_V8.blend',
    'Bagapieassets_database_V9.blend',
    'Bagapieassets_database_V10.blend',
    'Bagapieassets_database_V11.blend',
    'Bagapieassets_database_V12.blend',
    'Bagapieassets_database_V13.blend',
    'Bagapieassets_database_V14.blend',
    'Bagapieassets_database_V15.blend',
    'Bagapieassets_database_V16.blend',
    'Bagapieassets_database_V17.blend',
    'Bagapieassets_database_V18.blend',
    'Bagapieassets_database_V19.blend',
    'Bagapieassets_database_V20.blend',
    'blender_assets.cats.txt',
]
baga_packages = [
    'BagaPie_Assets_Free.baga',
    'BagaPie_Assets_Vol_1.baga',
    'BagaPie_Assets_Vol_2.baga',
    'BagaPie_Assets_Vol_3.baga',
    'BagaPie_Assets_Vol_4.baga',
    'BagaPie_Assets_Vol_5.baga',
    'BagaPie_Assets_Vol_6.baga',
    'BagaPie_Assets_Vol_7.baga',
    'BagaPie_Assets_Vol_8.baga',
    'BagaPie_Assets_Vol_9.baga',
    'BagaPie_Assets_Vol_10.baga',
    'BagaPie_Assets_Vol_11.baga',
    'BagaPie_Assets_Vol_12.baga',
    'BagaPie_Assets_Vol_13.baga',
    'BagaPie_Assets_Vol_14.baga',
    'BagaPie_Assets_Vol_15.baga',
    'BagaPie_Assets_Vol_16.baga',
    'BagaPie_Assets_Vol_17.baga',
    'BagaPie_Assets_Vol_18.baga',
    'BagaPie_Assets_Vol_19.baga',
    'BagaPie_Assets_Vol_20.baga',
]

###################################################################################
# INSTALL A NEW BAGAPIE PACKAGE
###################################################################################
class BAGAPIE_OT_install_package(Operator, ImportHelper):
    """Install a BagaPie Pack"""
    bl_idname = 'bagapie.installpack'
    bl_label = 'Install BagaPie Assets Pack'
    bl_options = {'REGISTER', 'UNDO'}

    
    filter_glob: StringProperty(
        default='*.baga',
        options={'HIDDEN'}
    )

    replace_pack:  bpy.props.BoolProperty(
        name="Replace pack",
        default=False
        )

    def execute(self, context):

        

        location_library = bpy.context.preferences.addons['Bagapie'].preferences.library_location
        filename, extension = os.path.splitext(self.filepath)
        add = True
        pack_to_install = []
        pack_already_installed = []
        
        new_file_path = filename + '.zip'
        os.rename(self.filepath, new_file_path)

        with zipfile.ZipFile(new_file_path, 'r') as zip_ref:
            for pack in zip_ref.namelist():
                if os.path.exists(location_library + ntpath.basename(pack)) and self.replace_pack == False:
                    print("Package already installed : " + pack)
                    pack_already_installed.append(pack)
                    # Warning(message = "Pack already Installed !", title = "INFO", icon = 'INFO')
                else:
                    pack_to_install.append(pack)


        if len(pack_to_install) > 0 or self.replace_pack == True:
            for del_pack in pack_already_installed:
                os.remove(location_library + ntpath.basename(del_pack))
            try:
                os.remove(location_library + ntpath.basename('blender_assets.cats.txt'))
            except:pass
            shutil.unpack_archive(self.filepath.replace(".baga", ".zip"), location_library)


            # for packages in pack_to_install:
            #     if ntpath.basename(packages) in baga_libraries:
            #         # if self.replace_pack == True:
            #         #     os.remove(location_library + ntpath.basename(packages))
            #         shutil.copy(self.filepath, location_library + ntpath.basename(filename) + '.zip')

            #     else:
            #         add = False
            #         print("This is not a BagaPie Package, or the name has been changed")
            # os.remove(location_library + ntpath.basename(filename) + '.zip')


            if len(pack_already_installed) > 0:
                Warning(message = str(len(pack_already_installed)) + " packs was already installed. New one(s) added.", title = "INFO", icon = 'INFO')
            else :
                Warning(message = "Pack Installed ! Take a look in the Asset Browser > BagaPie Assets.", title = "INFO", icon = 'INFO')



            prefs = bpy.context.preferences
            filepaths = prefs.filepaths
            asset_libraries = filepaths.asset_libraries

            for lib in asset_libraries:
                if lib.name == "BagaPie Assets":
                    add = False
            if add == True:
                file_path = bpy.context.preferences.addons['Bagapie'].preferences.library_location
                bpy.ops.preferences.asset_library_add(directory = file_path)
                asset_libraries[len(asset_libraries)-1].name = "BagaPie Assets"

            for lib in asset_libraries:
                if lib.name == "BagaPie Assets":
                    if lib.path != bpy.context.preferences.addons['Bagapie'].preferences.library_location:
                        lib.path = bpy.context.preferences.addons['Bagapie'].preferences.library_location

        else :
            Warning(message = "Pack already Installed !", title = "INFO", icon = 'INFO')

        new_file_path = filename + '.baga'
        os.rename(self.filepath.replace(".baga", ".zip"), new_file_path)

        return {'FINISHED'}



        # SI PAS DE LIBRAIRIE TROUVE L'AJOUTER





###################################################################################
# ASSET BROWSER
###################################################################################
class BAGAPIE_OT_set_package_location(Operator, ImportHelper):
    """Set BagaPie Package Location"""
    bl_idname = 'bagapie.setpacklocation'
    bl_label = 'New Location'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        location_original = bpy.context.preferences.addons['Bagapie'].preferences.library_location
        # filename, extension = os.path.splitext(self.filepath)

        base_path = os.path.dirname(self.filepath)
        location_new = os.path.normpath(base_path)
        if location_new[-1] != "\\":
            location_new = location_new + "\\"

        bpy.context.preferences.addons['Bagapie'].preferences.library_location = location_new

        for lib in baga_libraries:
            try:
                shutil.move(location_original + lib, location_new + lib)
            except: pass




        prefs = bpy.context.preferences
        filepaths = prefs.filepaths
        asset_libraries = filepaths.asset_libraries

        assets_lib_found = False
        for lib in asset_libraries:
            if lib.name == "BagaPie Assets":
                assets_lib_found = True

        if assets_lib_found == False:
            file_path = location_new
            bpy.ops.preferences.asset_library_add(directory = file_path)
            asset_libraries[len(asset_libraries)-1].name = "BagaPie Assets"
        else :
            for lib in asset_libraries:
                if lib.name == "BagaPie Assets":
                    lib.path = location_new


        return {'FINISHED'}



###################################################################################
# DISPLAY WARNING MESSAGE
###################################################################################
def Warning(message = "", title = "Message Box", icon = 'INFO'):

    def draw(self, context):
        self.layout.label(text=message)

    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)


classes = [
    BAGAPIE_OT_install_package,
    BAGAPIE_OT_set_package_location
]

