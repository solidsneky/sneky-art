# Baked Camembert 🧀
small Camembert wheel
oven safe pot
herbs and spices and honey or extra-virgin olive oil.

https://www.thepetitecook.com/how-to-make-baked-camembert/

Preheat oven to 180°C/(oven fan 160°C). Arrange a baking tray onto the middle shelf.
Remove the plastic packaging and arrange the Camembert back in its wooden box, or in a small baking pot.
Score a deep cross or a crosshatch pattern on the top rind of the cheese. Drizzle with *honey* or extra-virgin *olive oil*, and insert the leaves of one *sprig of rosemary* into the gaps.
Sprinkle with *chili flakes*, season with sea *salt* flakes and optional *black pepper*
Bake in the oven for 12-15 minutes.
Remove from the oven, and serve it straight away whilst it is still hot and creamy.