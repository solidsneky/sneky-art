# Blueberry cheesecake 🍰
https://www.devceuplotny.cz/foodblog/citronovy-cheesecake-s-jahodam
https://www.mimibazar.cz/recept.php?id=107296&user=336193


### Baking sheet and cool in frigde
- sušenky, 200 g
- máslo, 100 g (80 if Lotus)

### Mix in order
- tvaroh, 500 g
- lučina, 200 g
- cukr, 200 g

- kůra z citrónu, 1
- vanilka, ?

- smetana, 100 ml 

- vejce, 4

### Bang on the counter (bubbles)
### 160 °C till jelly
###"spodek formy obalte alobalem,  položte do hlubokého plechu a ten naplňte vodou do výšky cca 3 cm tak, aby voda nepřesahovala alobal"

### Topping
- blueberries, 375 g
- lemon juice, 2 tbsp
- sugar, 1/2 c
- vanilla, 1/2 tsp
- cornstarch, 1.5 tsp
- water, 2 tbsp
- . *cinnamon!*

### 2h v troubě + overnight v lednici


# Tips
- crust:butter = 100g:50g
- *lotus:butter = 100g:35-40g*
- suroviny na pokojovou teplotu
- *neotevírat troubu*

" *Jak odstraním cheesecake z formy?* Postup je celkem jednoduchý, jen můj popis je dlouhý. Jakmile je cheesecake dokonale vychladlý, ostrým nožem nebo krátkou cukrářskou paletou opatrně odřízněte cheesecake od krajů formy a odstraňte ji. Na cheesecake položte arch pečicího papíru a přiklopte velkým rovným talířem. Překlopte cheesecake (takže bude chvíli vzhůru nohama) a odstraňte dno formy i s pečicím papírem, kterým byla forma vyložená. Na dno umístěte ten talíř nebo tác, na kterém máte v úmyslu cheesecake servírovat (pozor, aby nebyl moc těžký a nerozdrtil cheesecake). Nyní koláč překlopte zpět, tedy aby byly sušenky zase dole, sejměte horní talíř a arch papíru a máte hotovo. Jestli budete cheesecake polévat další vrstvou (zakysanou smetanou, ovocným pyré…), nasaďte zpět bočnici formy, nechejte polevu ztuhnout a pak formu zase odřízněte pomocí ostrého nože."