# Chikin stir fry 🐤
# 1. Slice and Marinate Some Chicken
Slice them into 1-inch thick strips or 1-inch cubes and put it all in a bowl.

To season one pound of chicken, you need about 3 teaspoons of marinade. My basic stir-fry marinade consists of 
`equal parts soy sauce, rice wine, and corn starch`. So for one pound of chicken, I stir together a teaspoon soy sauce, a teaspoon of rice wine, and a teaspoon of cornstarch, then I jazz it up some `chili flakes or freshly grated ginger or garlic`. Once the marinade is all mixed up, pour it over the sliced chicken and toss it thoroughly so that every piece is coated. Then let it sit while you get everything else ready to stir-fry. (This is not a long marinade game—10 minutes will do the trick, and that's probably about as long as it'll take you to get all your ducks in a row before you can start cooking.)

# 2. Slice Some Vegetables
This where you can really play with variations in your chicken stir-fry game and add any quick-cooking vegetable you like. Snow peas and sugar snap peas are always great and don't need any slicing; bell peppers, broccoli, mushrooms and onions are also great, but require a little prep. Leafy things like bok choy or cabbage can also be used—the cabbage is best when thinly sliced. Carrots aren't quick cooking unless they're very thinly sliced, and then they're great—I like to use a mandolin to slice them into nice even little strips. You want about 2 cups of chopped vegetables for every pound of chicken.

# 3. Prepare a Sauce
Before you start cooking anything, make a sauce to stir in once everything's been seared. This is where the flavorings for the whole dinner will come from, so make it count—and make it strong. You want about 1/3 cup of sauce for every pound of chicken.

Start by whisking together some chicken or vegetable broth, soy sauce, and rice wine (about equal parts of each), then season it with freshly-grated ginger or garlic or hot sauce. Add a dash of fish sauce or a squeeze of lime juice or orange juice if you like. You can add a little sugar to the sauce (if you want it to be sweet) and a teaspoon of corn starch (if you want it to be thick). Whisk the sauce until it's smooth, have a taste, adjust it until it excites you, then set it aside and turn on your stove—you're ready to start cooking.

# 4. Get Your Pan As Hot as You Can
One of the secrets to a great chicken stir-fry is to get your pan as hot as possible. Whether you're using a wok or large cast-iron skillet, you want to set it over high heat until it's so hot that a drop of water dissolves instantly on its surface. Then, and only then, swirl a small amount of neutral high-heat cooking oil such as canola or grapeseed or peanut over the surface.

# 5. Sear That Chicken
As soon as you've swirled that oil into your smoking-hot pan, throw your marinated chicken pieces in there, in as even of a layer as you can manage while working as quickly as possible. You don't want any of the chicken to be piled on top of itself, let it lay for a minute to actually sear. After one minute, stir it all around until it's almost but not completely cooked through.

# 6. Add Your Vegetables
If your skillet or wok is feeling too dry, swirl a little bit more oil into it, then toss in all your carefully prepared vegetables and stir them around the hot surface for about a minute.

# 7. Plus Any Other Add-Ins You Want
When you add your vegetables you might also want to add some nuts for extra flavor and crunch—I love adding cashews or peanuts to my chicken stir-fry.

# 8. Finish With That Sauce You Made
Pour your sauce over everything and stir it around for about a minute. The sauce should bubble to a boil and start to thicken (you're still on high heat, right?). Once the sauce thickens, that's it—turn off the heat and sneak a little taste. If it needs some salt, stir some in.
Salted, roasted cashews steal the show every time. 

# 9. Serve Immediately (With Rice is Nice)
Stir-fry starts getting soggy and weird if it sits too long, so don't wait to eat it—serve it right away.