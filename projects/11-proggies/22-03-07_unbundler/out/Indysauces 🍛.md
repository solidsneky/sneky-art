# Indysauces 🍛
# Pro tips
## Chicken size
small and young chicken, 4 hours of marination works
larger sized or older chicken, at least 12 to 16 hours

## Curd
If you don't have hung curd, you can use thick curd too, but the cooking time might increase

## Adding cream
Turn the stove off while adding chilled cream to the gravy otherwise the gravy may curdle. 
This, or you can take a small amount of gravy in a separate bowl and add the cream. Mix well and then add it to the butter chicken. This way the cream will not curdle.

# Components
https://www.spiceitupp.com/how-to-make-indian-curry-indian-cooking-for-beginners/
https://www.spiceitupp.com/how-to-make-indian-curry-indian-cooking-for-beginners/
## Raw Ingredient
This is your main chosen protein or veg for the dish that is chicken, meat, fish, egg, vegetables, pulses or legumes.
## Aromatics
This is what makes the curry base and gives it the required consistency. They are onions, ginger and garlic.
## Spices
These are the flavour and taste enhancers. These ingredients are what makes each curry different from the other depending on the type of recipe you follow.
### Standard spices
Ground Cumin, ground coriander, turmeric powder, chilli powder and Garam Masala powder
whole spices such as; green cardamom, cloves, bay leaves and cinnamon.
## Heat Supplement
Since Indian food is normally associated with being hot that is the use of chillies, I classify this as a separate element of the dish.

## Sauce Base
Typically a curry sauce will be made of tomatoes.

However, you can also have different variants such as coconut milk, yogurt used either as the only ingredient to make a sauce base or you can also add half of each.

Water, of course, is must for cooking your chosen protein or veg.

## Herbs
These are the ingredients that add freshness and amp the flavour of your curry.

Herbs like fresh coriander leaves and dried Kasuri methi  (fenugreek leaves) added at the end of cooking not only adds to the taste but also the look of your dish.

## Fat


# #1 
https://recipes.timesofindia.com/us/recipes/kadhai-chicken/rs53523514.cms
250 grams chicken boneless
1/2 tablespoon coriander seeds
1/4 teaspoon fenugreek seeds
2 onion
1/2 tablespoon ginger paste
1/2 teaspoon red chilli powder
1 teaspoon salt
1/4 teaspoon garam masala powder
1/2 inch ginger
1/8 cup milk
4 tablespoon refined oil
1/2 tablespoon kasoori methi powder
2 red chilli
10 garlic
4 tomato
1 teaspoon coriander powder
1/8 teaspoon dry mango powder
1/4 cup coriander leaves
1 green chilli
1/2 cup Water
1/4 cup tomato puree
1/2 capsicum ( green pepper)
## Roast -> crush
coriander seeds

## Fry on low
fenugreek seeds
red chillies

onions
garlic

## Cook on high
ginger paste
coriander seeds
chilli powder
coriander powder
chicken (?)

chopped tomatoes
salt
mango powder
garam masala powder

## Cook with lid, 10 min
tomato puree

## Cook on low for a bit
coriander leaves
capsicum
tomato
ginger slices
green chillies

milk
water


# #2
https://recipes.timesofindia.com/recipes/butter-chicken/rs53205522.cms
1 tablespoon oil
1/2 teaspoon red chilli powder
3/4 cup tomato puree
1 teaspoon coriander seeds
1 cinnamon
2 and 1/2 green chilli
2 clove
250 grams butter
2 red chilli
1/2 teaspoon coriander powder
3/4 teaspoon kasoori methi powder
1 bay leaf
1 teaspoon salt
1 medium onion
2 handful dried fenugreek leaves
## Marinade (mix+overnight)
500 g	chicken

1 tsp	onion paste
1/2 tsp	garlic paste
1/4 cup	yoghurt (curd)
2		green cardamom
1/2 tsp	ginger paste
1/2 tsp	mace powder
1		black cardamom
1/4 tsp	sugar

## Then roast/cook til 3/4 done

## Sauté, 5 mins -> Puree
butter
bay leaves
cloves
cinnamon
red chillies
crushed coriander seeds

onion
chilli p.
coriander p.
kasoori methi p.
tomatoes

## Boil, simmer, boil
butter
pureed mixture

chicken
salt
fresh cream
water
(black pepper)

green chillies
crushed fenugreek leaves

## Garnish
2 tablespoon fresh cream
1/2 handful coriander leaves


# #3
1 large onion, chopped
1 tablespoon minced fresh ginger root
2 tablespoons minced garlic

1 teaspoon ground cinnamon
1 teaspoon ground black pepper
2 tablespoons ground coriander
2 tablespoons ground cumin
¼ teaspoon ground turmeric
1 teaspoon cayenne pepper

2 tomatoes
2 serrano chile peppers, seeded
½ cup fresh cilantro
½ cup yogurt, whisked until smooth
3 cups water

## Sauté
butter
onion

ginger
garlic
minced serrano

tomatoes (diced)
all of the spices

## Immersion blend
2 1/2 c water

## Add chicken, cook for a bit

## Whisk in
yogurt