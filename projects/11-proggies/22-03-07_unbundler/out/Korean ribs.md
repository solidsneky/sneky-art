# Korean ribs
***RECIPE, SERVES 4-6 PEOPLE***

1.4-2.3 kg beef short ribs, either flanken cut or English cut

1 pear, ideally an Asian variety
1 bunch of scallions

a few garlic cloves
a thumb of ginger
120 ml soy sauce
"couple TBSP" rice vinegar
"couple TBSP" sugar
"couple drops" sesame oil

toasted sesame seeds for garnish
a couple bunches of any dark, leafy green, washed and roughly chopped (optional side)
rice for side

# The day before
### Prep meat
If using English-cut short ribs, butterfly them per the video. If using flanken cut, consider washing them in a bowl of water to get off any bone fragments.

### Make marinade
Peel the *garlic and the ginger*. Peel and core the *pear*. Cut the *white parts off the scallions* and reserve the green parts for garnish later. Either grate all of the above, or put it in a food processor, along with about 1/2 cup (120 ml) *soy sauce* and a couple tablespoons each of *sugar* and *vinegar*. When everything is combined and smooth, taste it. It should taste good, but too sweet/salty/acidic. 

Consider adding more sugar, vinegar, soy sauce, etc, and put in a few drops of *sesame oil* to taste (it can go bitter if it gets too much abuse in the food processor, hence adding it toward the end). 

### Fridge overnight
Combine the marinade with the meat in a sealable container, along with just enough *water* to help the marinade coat and cover everything.

# On the day
Remove the meat from the marinade and scrape off as much from the surface as you can. 

Place the meat in a single layer in your widest pan. (You may need to cook in two batches, or use two pans.) Pour in enough water to come halfway up the meat, then turn the heat on high.

Flip the meat once or twice as the water boils out. As soon as most of the water is gone, you'll notice things starting to brown rapidly. Turn the heat way down and don't let anything burn. Flip the meat frequently and let it gently brown on all sides before taking it out to rest.

If making the greens, put as many of them in the pan as you can fit. They'll wilt quickly, so you'll be able to get in more as they shrink. Pour in some of the leftover marinade. Cook, stirring constantly, until they're just soft.

Slice up the ribs, serve them with rice and greens, garnish them with sesame seeds and the thinly sliced scallion tops.

https://www.youtube.com/watch?v=qsf8RlQW8Hw&t=61s