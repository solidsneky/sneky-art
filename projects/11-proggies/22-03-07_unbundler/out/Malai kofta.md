# Malai kofta

500 g mletého hovězího,
olej,
menší cibule,
1-2 stroužky česneku,
250 ml plnotučného mléka,
10 ml smetany

3/4 lžičky mleté skořice,
sůl, pepř,
špetka tymiánu,
4 lžičky mletého kardamomu, 
4 hřebíčky,
4 lžičky strouhaného muškátového oříšku 


Maso ochutíme solí, pepřem, kardamomem a tymiánem

Vytvoříme malé knedlíčky

Osmažíme na oleji

V použitém oleji osmahneme jemně nakrájenou cibulku Přidáme kousky česneku, skořici a hřebíček Zalijeme hrnkem vody a necháme dusit, dokud nebude česnek měkký

Přidáme masové knedlíčky a dusíme dalších 8-10 minut Zalijeme mlékem a smetanou a vsypeme muškátový oříšek

Důkladně prohřejeme

मलाई कोफ्ता
malai smetana/kofta = masové knedlíčky