# Moroccan heart stew 🍲

https://thenourishedcaveman.com/moroccan-heart-stew/

½ teaspoon ground cardamom
½ teaspoon ground cinnamon
½ teaspoon ground cayenne
½ teaspoon ground clove

1 kg beef heart meat, cut in 2.5 cm cubes
500 g beef stew meat

3 cups sliced onion
2 tablespoons peeled, chopped fresh ginger

2 cups water or bone broth
1½ cup dried unsulfured apricots, quartered
1 lemon cut in rounds
salt to taste


> The Night before:
Marinate heart meat (whole or cubed) in ½ cup of whey or 2 tablespoons of apple cider vinegar.

> On the day:

### Saute
> in big wok pan, will boil in it later
- *onion/ginger* 

- *spices*

### Brown on high flame
> on big stainless pan
- all the *meat*
- dividing it into small batches so that it just quickly sears the surface.


Now add the meat to the onion mix, and stir 
well to coat with the spices.

### Deglaze
- *water/broth*


Now pour the deglazing liquid on top of the meat, add the second cup of broth, the sliced *lemon* and a good pinch of *salt.*

### Boil > Simmer, 1.5 h
- until heart is very tender

### Simmer, 10 min
- *apricots*
- until liquid is thickened and reduced.


Serve with rice pilaf and a salad.