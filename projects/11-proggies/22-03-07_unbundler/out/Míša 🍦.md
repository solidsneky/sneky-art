# Míša 🍦
250 g tučného tvarohu (prostě 1 kostka)
250 g zakysané smetany (tj. 1 kelímek)
60 g cukru krupice (nebo 50g + 10 g vanilkového cukru)
1 lžička vanilkové pasty
250 g čokolády + 1 velká lžíce másla - na rozpuštění čokolády pro obalení nanuků
1. Tvaroh se smetanou, cukrem a vanilkou mixuji spolu, dokud nevznikne hladký krém - minimálně 3 minuty.

2. Připravený krém pomocí vidličky napěchuju do formičky a snažím se dávat pozor, aby nevznikaly žádné bubliny. Dám zamrazit na 2-3 hodiny.

3. Čokoládu s trochou másla a případně i mléka dám rozehřát na vodní lázeň a přesunu do nějaké úzké a vysoké nádoby - hrnek nebyl zcela ideální - a udržuji v teple. Čokolády by mělo být tolik, aby výška sloupce byla nepatrně vyšší než výška nanuku.