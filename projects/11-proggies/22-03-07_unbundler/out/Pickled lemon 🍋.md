# Pickled lemon 🍋
[link](https://www.daringgourmet.com/how-to-make-preserved-lemons-moroccan-middle-eastern-cooking/#wprm-recipe-container-58044)
INGREDIENTS
3 lemons per pint-sized jar
5-6 t salt sea salt or kosher recommended
An extra lemon for juicing
Water that has been boiled and cooled- sterile

INSTRUCTIONS
You can make however many preserved lemons you like, but roughly 3 will fit per pint-sized jar.

Thoroughly clean the lemons. Organic is recommended. If you can't find organic, let the lemons sit in some vinegar water for a few minutes, then rinse.

Trim the nubs off both ends of each lemon. Quarter each lemon, slicing them down just over 3/4 of the way to leave the slices attached at the end.

Put one teaspoon of salt into the cavity of each lemon.

Place one teaspoon salt into the bottom of the jar. Put a lemon in the jar, cut-side down, pressing firmly to squish out the lemon juice. Put a teaspoon of salt on top of the lemon. Firmly press the second lemon down on top of the first lemon. Repeat with the third lemon, pressing down firmly. Add a teaspoon of salt on top of the lemon.

The jar should be halfway full with lemon juice. If needed, squeeze some additional lemon juice into the jar to bring it to the halfway point. Don't waste that lemon; slice it and stuff the slices into the jar. Pour the boiled/cooled water into the jar to fill it to the top.

Screw the lid on and let it sit at room temperature for 3 days, shaking it and rotating the jar upside-down/right-side up a few times per day. After 3 days transfer the jars to the refrigerator and let them sit for at least 3 weeks before using. Store in the fridge, will keep for at least 6 months (see Note).

NOTES
*In most countries preserved lemons are not stored in the refrigerator, they're simply kept in a cool, dark place. I've added the recommendation to store in the fridge based on USDA guidelines. Store them according to your own preference.
*Whatever dish you use them in, discard the pulp (it's the peel that is used) and thoroughly wash the peel to remove excess salt.
*USING OTHER CITRUS FRUITS:  You can also make preserved limes, oranges, grapefruit and kumquats.  The process is identical but because high acidity is required for proper preservation you will still need to top off the jars with lemon juice.

> or
[link](https://cooking.nytimes.com/recipes/1016212-preserved-lemons#:~:text=Fit%20all%20the%20cut%20lemons,jar%20until%20juice%20covers%20everything.)

9 organic lemons
 Kosher salt
1 heaping teaspoon black peppercorns
2 bay leaves
(cinnamon)
(cardamom)

Scrub 3 to 5 organic lemons, enough to fit snugly in a medium jar with a tight-fitting lid (have 2 to 4 more ready on the side). Slice each lemon from the top to within 1/2 inch of the bottom, almost cutting them into quarters but leaving them attached at one end. Rub kosher salt over the cut surfaces, then reshape the fruit. Cover the bottom of the jar with more kosher salt. Fit all the cut lemons in, breaking them apart if necessary. Sprinkle salt on each layer.
Press the lemons down to release their juices. Add to the jar the peppercorns and bay leaves, then squeeze the additional lemons into the jar until juice covers everything.
Close the jar and let ripen at cool room temperature, shaking the jar every day for 3 to 4 weeks, or until the rinds are tender to the bite. Then store it in the refrigerator.
To use, remove a piece of lemon and rinse it. (Add more fresh lemons to the brine as you use them up.) The minced rind is added at the very end of cooking or used raw; the pulp can be added to a simmering pot.


[link](https://www.feastingathome.com/how-to-preserve-lemons/#tasty-recipes-35922-jump-target)
Ingredients
4 lemons (4-5 ounces each) Meyer Lemons are nice here but not imperative.
1–2 tablespoons sea salt
a clean, 2-3 cup wide mouth jar with lid
optional additions: bay leaves, peppercorns, other whole spices.
Instructions
Clean a 2-3 cup jar with hot soapy water, dry.
Slice 2 lemons into 1/8-1/4 inch thick disks, about 8 slices each. (Use the ends for juicing)
Salt the bottom of the jar with 1/4 teaspoon salt and begin layering the lemons, salting each slice or layer with a scant 1/4 teaspoon salt leaving at least 1 inch of room at the top of the jar. You’ll need about 2 teaspoons sea salt, per 1 large ( 4-5 ounce) lemon.
Once the jar is filled with the salted sliced lemons, press them down either with your fingers or a muddler, compressing them, then squeeze the juice from the remaining 1-2 lemons to completely submerge the slices, again pressing down with your fingers. Weight the lemons down under the brine. You can use a fermentation weight, a sterilized river stone (boil first), or a small ziplock filled with water to keep them submerged. Cover.
Place this in a cool dry place for at least 1 week, feel free to shake the jar periodically, always making sure lemons are submerged under the lemony salty brine. After about a week they should start to look translucent and the peel should become very tender. I’ll often let them go 2- 3 weeks for extra tender. If you notice mold at the top, it is likely because lemons were exposed to air– not to worry. Just remove that top layer and make sure the remaining lemons are submerged under the brine.
Once translucent and tender, refrigerate! ( see photo above).
Notes
Lemons will last indefinitely in the fridge (up to or over a year) as long there is brine covering them.
Use them chopped up in dressings, marinades, salads and grain or bean dishes, or whiz them up in the blender and make a paste to add to soups, stews, dressings, etc. Use the “syrup” to season soups and stews.


[link]https://jj-kitchen.tokyo/recipes/preserved_lemons/
Ingredients
Servings: about 500g (1 pint) of preserved lemons

4-5 lemons (preferably oganic Meyer lemons)
4 tbsp sea salt (10-20% of the weight of the lemons)
Instructions
To sanitize the jar, pour boiling water into the jar. Drain and air dry.
Wash lemons well. Trim the nubs off both ends of each lemon. Cut them into 8 pieces (a classic recipe says you cut the lemons into quarters, leaving the ends attached but I prefer to cut them smaller as I chop them when I use anyway.) You may also slice the lemons. That’s up to you.
Put a teaspoon of salt in the bottom of the jar. Place lemons in a layer and add another teaspoon of salt over them. Repeat the process.
Press the lemons firmly with the bottom of a wooden spoon to pack them down and start the juices flowing. The jar should be half full of juice from the lemons. If needed, squeeze some extra lemon juice into the jar.
Cover the jar and let stand on the counter for 3 days, giving it a shake and turn it upside-down a few times a day.
Let the lemons ferment on the counter for 3-4 weeks, until they are ready to use.
Once ready to use, the lemons can be stored in the refrigerator for 12 months.

> more links
[link](https://www.npr.org/2013/04/08/176577903/preserved-lemons-older-wiser-and-full-of-flavor?t=1618310645896)
[link](https://toirokitchen.com/blogs/recipes/extra-umami-preserved-lemon)