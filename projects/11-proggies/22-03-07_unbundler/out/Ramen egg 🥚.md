# Ramen egg 🥚
- eggs, 2-4
- soy sauce, 2 T
- mirin, 2 T
- water, 3-6 T

### Boil -> Simmer, 6-9 min
- Rotate eggs for centered yolk

### Marinade up to 4 days