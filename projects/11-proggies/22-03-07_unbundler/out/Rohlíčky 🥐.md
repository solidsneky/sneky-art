# Rohlíčky 🥐
400 g hladké mouky
100 g najemno nastrouhaných ořechů a mandlí
100 g cukru krupice
100 g másla
100 g sádla
1 vejce

vanilkový cukr na obalení
moučkový cukr na obalení

1
Veškeré suroviny smícháme a zpracujeme v tuhé těsto, zabalíme je do potravinářské fólie a necháme přes noc odpočinout v lednici.
2
Troubu rozehřejeme na 160 °C. V míse smícháme cukr krupici, moučkový a vanilkový cukr.
3
Z těsta ukrojíme část a vyválíme ho do válečku. Z něj krájíme malé kousky, které vyválíme do malých válečků a vytvarujeme do rohlíčků. Pečeme je v rozehřáté troubě dorůžova. Upečené rohlíčky ihned obalujeme ve směsi cukrů
TIP
Použijte směs vlašských a lískových ořechů. Vlašských dejte víc, mají výraznější chuť. Mandle naopak chuť zjemní.