# Thai basil chicken
##Ingredients
225g / 7oz chicken thigh fillets , skinless boneless, cut into bite size pieces
1 green onion , cut into 4cm / 2" lengths.
1 cup Thai basil leaves , loosely packed (Holy Basil if you can find it) (Note 1)
2 garlic cloves , large, finely chopped (Note 2)
1 birds eye or Thai chilli , deseeded and finely chopped
1 1/2 tbsp oil (peanut, vegetable or canola)

###Sauce
2 tsp oyster sauce
1 tsp light soy sauce (Note 3)
1 tsp dark soy sauce (or all purpose) (Note 3)
1 tsp sugar
2 tbsp water

Serving
Steamed jasmine rice

##Instructions
Put Sauce ingredients in a small bowl and mix to combine.

Heat oil in wok or pan over high heat.

Add garlic and chilli and cook for 10 seconds. 

Don't inhale - the chilli will make you cough!
Add the white part of the green onions and chicken and fry until cooked, around 2 minutes.

Add Sauce and cook for 1 minute until the water reduces to make a thick glossy sauce.
Toss through green part of green onions and basil leaves. Stir until just wilted, then serve immediately with steamed jasmine rice.


https://www.recipetineats.com/thai-basil-chicken-stir-fry/
Notes
2. Garlic - Finely chopping the garlic rather than minced it (or using jarred garlic) stops it from burning quickly and spitting when it hits the hot wok.

3. Soy sauces - can sub light soy sauce or both the light and dark soy with ordinary all purpose soy (like Kikkoman). Or can use just light soy sauce. Flavour not quite as intense as it should be and colour will be paler, but still super tasty.
Do not use JUST dark soy sauce, flavour will be too intense.

4. Serving size - This recipe makes one giant serving or 2 reasonable sized servings. Complete the meal with a simple side of juicy slices of cucumber and tomato with no dressing - this is very Thai! Refreshing accompaniment to spicy Thai food.
