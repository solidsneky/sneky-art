# Tomato soup 🍅🍲
1 large onion
1 fennel bulb (you could skip this, or replace it with another onion)
1 stick (4 oz, 113g) butter
pepper
1/4 cup flour (30g)
2 28 oz (800g) cans of tomatoes (quality matters a lot)
1 cup (237mL) white wine (very optional)
salt
water
sugar (if the tomatoes aren't sweet enough)
tomato paste (if the tomatoes aren tomatoey enough)


Roughly chop the onion and the fennel bulb (reserving the stalks and fronds for later). 
Melt the butter in a big pot over medium heat and cook the onion and fennel in there for a few minutes until it starts to soften. 
Grind in some pepper and put in the celery seeds. Stir in the flour, and cook it for a couple minutes. 
Before anything browns, dump in the tomatoes. If the tomatoes are whole, you can accelerate their cooking by squishing them up. 
Stir in the wine, if you're using it. Simmer for at least a half hour, stirring and scraping occasionally to keep anything from burning on the bottom.

For the chili oil garnish, put maybe 1/4 cup (60mL) of olive oil in a small pan and fill it with chili flakes. Drop in the garlic clove and heat it until the garlic just starts to sizzle. Leave it on low heat to infuse while the soup cooks.

When the soup is ready, puree it and then add salt and water to taste — you will probably need a lot of both. Consider the addition sugar or tomato paste to enhance the flavor, or maybe some vinegar if you didn't use the white wine. If you want it super smooth, use a stiff spoon to grind the soup through a sieve, discarding the vegetable fibers.

Serve the soup with a drizzle of chili oil on top and maybe some of the reserved fennel fronds.