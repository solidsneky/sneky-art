# koblížkyyy!! x3 xOO
https://www.rohlik.cz/chef/1851-tvarohove-koblizky-s-ovocnym-dipem?utm_source=blog&utm_medium=page&utm_campaign=masopust-w06&utm_content=tema

SUROVINY NA KOBLÍŽKY
250 g tvarohu v kostce
3 vejce
85g/6 lžic plnotučného mléka
3 lžíce rumu
1 prášek do pečiva
220 g hladké pšeničné nebo špaldové mouky
1 l oleje na smažení

NA OBALENÍ
1 skořicový cukr
Cca 100 g cukru krupice

NA TVAROHOVÝ KRÉM
250 g tučného tvarohu
1 vanilkový cukr
30 g cukru krupice
150 g 40% šlehačky
40 g bílého jogurtu 


Smícháme v míse všechny suroviny na koblížky (mouku s práškem do pečiva smícháme nejprve zvlášť) krom oleje.

Rozpalte olej tak, že bude zřetelně vytvářet nitky či mapy – olej musí po vložení těsta praskat a vytvářet kolem koblížku bublinky

Smažte několik minut dozlatova po všech stranách

Teplé, ale ne už horké, vložte do směsi obou cukrů na obalení

Na krém vyšlehejte šlehačku. Smíchejte tvaroh s jogurtem a cukry a nakonec stěrkou vmíchejte šlehačku. Naplňte do zdobícího sáčku, 
udělejte malou dírku (buď zdobící špičkou nebo třeba koncem malé vařečky) a stříkněte dovnitř krém