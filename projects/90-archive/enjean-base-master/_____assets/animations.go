package assets

import (
	"encoding/json"
	"log"
	fpaths "path/filepath"

	ebi "github.com/hajimehoshi/ebiten/v2"

	. "jam/helpers"
)

var LOG_ASSET_RELOADS = true

/**  REWRITE:
  We don't have a good way to manage multiple animations
  in one file -- like we want to do when working with layers.

  There should be one more layer of 1:N connection between
  a single spreadsheet and multiple animations via layers.
  At the moment, each layer is a new animation that has
  to be reloaded -- we're reading the same file N times
  for every layer, and thus animation, it contains.

  I would definitely prefer having some kind of store that
  the user doesn't have to know about that would hold
  these things as a system instead of objects.
                                         -- MK 2021-03-17
*/

// @todo; Add a way of adding sound-trigger data into the animations

// Note: Aseprite assets have two key files, the json and the png.
// The json includes all data about frame regions and tags (what regions
// belong to which animation).
// The json also has the filepath of the png, relative to the json file.

type (
	AnimData struct {
		Asset                // Watchable and reloadable at runtime
		Tags  map[string]Tag // Sub-animations included in the asset
		Sheet *ebi.Image     `json:"-"` // Spritesheet with image data
	}
	Tag struct {
		Name   string  // Name of the tag as defined by the Aseprite export
		Frames []Frame // Frame data containing the image
	}
	Frame struct {
		DurationSecs SecsDT     // Duration for the frame
		SubImage     *ebi.Image `json:"-"` // Sheet region for this frame
	}
)

func (an *AnimData) TagByName(tagName string) (Tag, bool) {
	t, isInMap := an.Tags[tagName]
	return t, isInMap
}
func (an *AnimData) TagDefault() Tag {
	return an.Tags[ANIMATION_TAG_DEFAULT]
}

func (t Tag) TotalDuration() (dur SecsDT) {
	for _, f := range t.Frames {
		dur += f.DurationSecs
	}
	return dur
}

func LoadAnimation(abspath string) *AnimData {
	var a = AnimData{
		Asset: Asset{WatchedFilepath: abspath},
	}
	a.Reload()
	AssetWatcher.Add(&a) // Watch every anim loaded this way
	return &a
}

func (a *AnimData) Reload() {
	defer a.RecordLoaded() // Should record even failed reloads

	// When reloading, don't panic under any circumstances.
	// If the reload fails, don't just crash the whole thing
	// but just log that it happened.
	// The only crash-on-load is when the game starts.

	err := a.reload()
	if err != nil {
		// TODO:  Colorize
		log.Printf("[ERRO] animation reload: %v", err)
	}

	if LOG_ASSET_RELOADS {
		log.Printf("RELOADED: `%s`", a.WatchedFilepath)
	}
}
func (a *AnimData) reload() error {
	var watchPath = a.Asset.WatchedFilepath

	aseJson, err := loadAseJSON(watchPath)
	if err != nil {
		return err
	}

	sheetPath := fpaths.Join(fpaths.Dir(watchPath), aseJson.Meta.Image)
	img, err := loadImage(sheetPath)
	if err != nil {
		return err
	}

	a.Sheet = img
	a.Tags = defineTags(aseJson, a.Sheet)

	return nil
}

func loadAseJSON(path string) (j aseJSON, err error) {
	jsonBytes, err := loadBytes(path)
	if err != nil {
		return j, err
	}
	err = json.Unmarshal(jsonBytes, &j)
	if err != nil {
		return j, err
	}
	return j, nil
}
