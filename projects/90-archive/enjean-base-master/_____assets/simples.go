package assets

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	"image/color"

	. "jam/helpers"
)

func DebugAnimation(w, h int, c color.Color, __e Entity) *AnimationPlayer {
	img := func() *ebi.Image {
		im := ebi.NewImage(w, h)
		im.Fill(c)
		return im
	}()
	return NewAnimationPlayer(&AnimData{
		Asset: Asset{},
		Tags: map[string]Tag{
			ANIMATION_TAG_DEFAULT: {
				Name: ANIMATION_TAG_DEFAULT,
				Frames: []Frame{{
					DurationSecs: 1,
					SubImage:     img,
				}},
			}},
		Sheet: img,
	}, __e.ID)
}
