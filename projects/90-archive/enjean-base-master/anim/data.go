package anim

import (
	ebi "github.com/hajimehoshi/ebiten/v2"

	. "g/helpers"
)

// Note: Aseprite assets have two key files, the json and the png.
// The json includes all data about frame regions and tags (what regions
// belong to which animation).
// The json also has the filepath of the png, relative to the json file.

type (
	Data struct {
		Tags  map[string]Tag // Sub-animations included in the asset
		Sheet *ebi.Image     `json:"-"` // Spritesheet with image data
	}
	Tag struct {
		Name   string  // Name of the tag as defined by the Aseprite export
		Frames []Frame // Frame data containing the image
	}
	Frame struct {
		DurationSecs Secs       // Duration for the frame
		SubImage     *ebi.Image `json:"-"` // Sheet region for this frame
	}
)

func (an *Data) TagByName(tagName string) (Tag, bool) {
	t, isInMap := an.Tags[tagName]
	return t, isInMap
}
func (an *Data) TagDefault() Tag {
	return an.Tags[ANIMATION_TAG_DEFAULT]
}

func (t Tag) TotalDuration() (dur Secs) {
	for _, f := range t.Frames {
		dur += f.DurationSecs
	}
	return dur
}
