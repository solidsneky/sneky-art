package main

import (
	"log"

	"os"
	"path/filepath"
	"strings"

	"unicode" // Just for the uppercasing of one single letter
)

const SEPA = string(filepath.Separator)

func parseDir(dirpath, packageName string) (files struct{ prod, dev []byte }) {

	if packageName == "" {
		packageName = filepath.Base(dirpath)
	}

	log.Printf("  Reading files...")
	fpaths := collectFilesFromDirectory(dirpath)

	log.Printf("  Grouping and converting names...")
	prepped := prepare(fpaths)
	// TODO:  Group files by extension, maybe.

	log.Printf("Generating file data... ")
	{
		data := templateData{
			Files:       prepped,
			PackageName: packageName,
		}
		files.prod = templateExecute(templateEmbed, data)
		files.dev = templateExecute(templateDev, data)
	}
	return files
}

//

type preparedNames struct {
	Path    string // Original full file path of the file, relative for embedding.
	Varname string // Swapped characters to be a valid Go variable name

	//
	// The following fields are not used yet for templating.
	// I'm mainly leaving them to show the way I'd do it but I'm not gonna
	// do the full thing right now...
	Base      string   // Filename without the extension (doesn't include dot)
	Extension string   // Extension such as .json, .png, .wav (doesn't include dot)
	Parents   []string // Parenting directories, farthest first.
}

func prepare(fpaths []string) (prep []preparedNames) {

	for _, f := range fpaths {

		if f[0] == '.' {
			continue // no dotfiles
		}

		n := preparedNames{

			// The generated files should have unix @slashes
			Path: filepath.ToSlash(f),

			Varname: func(fpath string) (varname string) {
				varname = fpath
				{
					varname = strings.ReplaceAll(varname, SEPA, "__")
					varname = strings.ReplaceAll(varname, ".", "_")
					varname = strings.ReplaceAll(varname, "-", "_")
					varname = strings.ReplaceAll(varname, " ", "_")
					// NOTE:  Obviously, other weird symbols are not good, but these are
					//   the most common.
					// TODO:  Do something about that, would ya?!

				}
				{ // Uppercase the first letter to make the variable name global
					b := []rune(varname)
					b[0] = unicode.ToUpper(b[0])
					varname = string(b)
				}
				{
					varname = filepath.ToSlash(varname)
				}
				return varname
			}(f),
			Extension: func(fpath string) string {
				if ext := filepath.Ext(fpath); len(ext) == 0 {
					return "" // files without an extension return empty
				} else {
					return ext[1:] // get that dot outta here~
				}
			}(f),
			Base:    filepath.Base(f),
			Parents: strings.Split(filepath.Dir(f), SEPA),
		}

		if strings.HasSuffix(n.Varname, "_") {
			continue // Does not start with a letter that can be uppercased
		}

		switch n.Extension {
		case "go", "git", "gitignore":
			continue // Exclude source code that shouldn't end up in embed files
		}

		prep = append(prep, n)
	}

	return prep
}

//

func collectFilesFromDirectory(dirpath string) (filenames []string) {

	err := filepath.Walk(
		dirpath,
		func(fpath string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil // Nothing to do for arriving at a directory
			}

			// Cut off the dirpath from the path we use as embed directives
			// and filepaths. The paths are always relative to the generated
			// source code files -- whether we use embedding or not.
			withoutDirpath := strings.Split(fpath, dirpath+SEPA)[1]

			filenames = append(filenames, withoutDirpath)
			return nil
		},
	)
	if err != nil {
		panic(err)
	}

	return filenames
}
