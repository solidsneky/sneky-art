package main

import (
	"bytes"
	tem "text/template"
)

type templateData struct {
	Files       []preparedNames
	PackageName string
}

func templateExecute(t *tem.Template, data templateData) []byte {
	// ..just making sure the argument type is correct..
	var buf bytes.Buffer
	err := t.Execute(&buf, data)
	if err != nil {
		panic(err)
	}
	return buf.Bytes()
}

//
// Template using embed to provide files.
//
var templateEmbed = tem.Must(tem.New("").Parse(`// +build js production

package {{ .PackageName }}

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ EMBED MODE ]
    Filesystem embedded into the executable itself for more
    compact production builds.
*/

import (
	df "g/files"

	"embed"
)

var Files = df.NewDirfiles(fsEmbed, []string{
  {{- range .Files }}
	{{ .Varname }},
  {{- end }}
})

// Filenames listed as constants for code autocompletion
const (
  {{- range .Files }}
	{{ .Varname }} = "{{ .Path }}"
  {{- end }}
)

  {{ range .Files }}
//go:embed {{ .Path }}
  {{- end }}
var fsEmbed embed.FS

`))

//
// Template providing an `fs.FS` directory mapping to a real directory.
//
var templateDev = tem.Must(tem.New("").Parse(`// +build !js,!production 

package {{ .PackageName }}

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ RUNTIME MODE ]
    Filesystem mapped to the real files on disk. These files
    are expected to change.
*/

import (
	df "g/files"

	"os"
	. "g/helpers"
)

var fsReal = os.DirFS(ThisSourceFileDir())

var Files = df.NewDirfiles(fsReal, []string{
  {{- range .Files }}
	{{ .Varname }},
  {{- end }}
})

// Filenames listed as constants for code autocompletion
const (
  {{- range .Files }}
    {{ .Varname }} = "{{ .Path }}"
  {{- end }}
)
`))
