package helpers

import (
	"fmt"
)

var Err = fmt.Errorf
var USER_EXIT = Err("User Exited.")

func Assert(mustBeTrue bool, panicValue string) {
	if !mustBeTrue {
		panic("assert: " + panicValue)
	}
}

func CanPanic(err error) {
	if err != nil {
		panic(err)
	}
}
