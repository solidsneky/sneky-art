package helpers

import (
	ebi "github.com/hajimehoshi/ebiten/v2"
	vec "github.com/hajimehoshi/ebiten/v2/vector"
)

//
//  TODO:  Move to a good file, export and use in x/ where relevant
//

var whiteImage *ebi.Image = func() *ebi.Image {
	img := ebi.NewImage(1, 1)
	img.Fill(BasicColor.White)
	return img
}()

func setColor(c Color, vs []ebi.Vertex) {
	r, g, b, a := c.RGBA_01_32()
	for i := range vs {
		vs[i].ColorR, vs[i].ColorG, vs[i].ColorB, vs[i].ColorA = r, g, b, a
	}
}

//
//  >>> end todo
//

type FPSMeter struct {
	fpsEachFrame []float32
	head         int
	keepNFrames  int
}

func (m *FPSMeter) Init(keepNFrames int) *FPSMeter {
	m.keepNFrames = keepNFrames
	m.fpsEachFrame = make([]float32, m.keepNFrames)
	m.head = 0
	return m
}

func (m *FPSMeter) EachFrame() []float32 {
	a := m.fpsEachFrame[:m.head] // before head
	b := m.fpsEachFrame[m.head:] // after head
	return append(a, b...)
}

func (m *FPSMeter) Snap() {
	dtThisFrame := Dt
	m.ManualSnap(dtThisFrame) // Taking the global from helpers.
}
func (m *FPSMeter) ManualSnap(dt Secs) {
	m.head++
	if m.head >= len(m.fpsEachFrame)-1 { // is head out of bounds before setting?
		if len(m.fpsEachFrame) < m.keepNFrames { // make sure the len is correct
			m.fpsEachFrame = make([]float32, m.keepNFrames)
		}
		m.head %= m.keepNFrames // wrap
	}

	var fps float32
	if dt == 0 {
		fps = 0
	} else {
		fps = 1 / float32(dt)
	}
	m.fpsEachFrame[m.head] = fps
}

func (m *FPSMeter) Draw(dest *ebi.Image, x, y, w, h int) {

	const FPS_TARGET = 60 // Don't guess this  @vsync_real_framerates
	const FPS_BAD = 43
	const GRAPH_MAX float32 = FPS_TARGET * 2

	// Sometimes, Ebiten does a double-update, the FPS detected
	// is then in [200..6000] fps range. These usually happen
	// after a lag to "catch up" and give you an update per every
	// frame. These lags are nothing we can really affect,
	// there's always some noise in the timings.
	const OVER_THE_LIMIT = 200

	// TODO:  To make the drawing really clean when width is not the same,
	//   as number of kept frames (or its int multiply),
	//   we need to make changes somewhere, here's some options:
	//
	//  - Make the width be set by kept frames, API then gives that width
	//    and lets the caller decide.                   [easy, but not for caller]
	//
	//  - We set the keptFrames to the draw width, react to those changes at runtime,
	//    and always draw and keep the number for drawing.           [almost done]
	//
	//  - Do the above and overshoot keptFrames by some number anyway
	//    and only draw ones that can fit -- who says we need to stop tracking
	//    at exactly how much we use there.
	//    It's just a bunch of floats anyway.                  [probably the best]

	// TODO:  Draw from one point, don't circle around. That's an implementation
	//   detail and it doesn't contribute to the visual information much.
	//   Or wrap it around and dupe! The only issue is the wrap-around not being
	//   joined to the rest.                                  [...another option?]

	X, Y, W, H := float32(x), float32(y), float32(w), float32(h)
	var (
		frames   = m.EachFrame()
		colWidth = W / float32(len(frames))
	)

	// The frame column
	for i, fps := range frames {
		var (
			overLimit = fps >= OVER_THE_LIMIT
			bad       = fps <= FPS_BAD
		)
		var (
			bottom = Y + H
			top    = bottom - (fps / GRAPH_MAX * H)
			I      = float32(i)
			left   = X + (I * colWidth)
			right  = left + colWidth
		)
		if overLimit {
			top = Y
		}
		p := &vec.Path{}
		p.MoveTo(left, top)
		p.LineTo(right, top)
		p.LineTo(right, bottom)
		p.LineTo(left, bottom)
		p.LineTo(left, top)
		vs, is := p.AppendVerticesAndIndicesForFilling(nil, nil)
		if i != m.head {
			switch {
			case bad:
				setColor(BasicColor.Red, vs)
			case overLimit:
				setColor(BasicColor.Red.Opacity(0.2), vs)
			default:
				setColor(BasicColor.Blue, vs)
			}
		}
		dest.DrawTriangles(vs, is, whiteImage, &ebi.DrawTrianglesOptions{})
	}

	fpsLine(dest, X, Y, W, H, GRAPH_MAX, FPS_TARGET, BasicColor.Green)
	fpsLine(dest, X, Y, W, H, GRAPH_MAX, FPS_BAD, BasicColor.Red)
}

func fpsLine(dest *ebi.Image, X, Y, W, H, graphMax, fps float32, color Color) {
	g := ebi.GeoM{}
	g.Scale(float64(W), 1)
	g.Translate(float64(X), float64(Y+H-(fps/graphMax*H)))
	c := ebi.ColorM{}
	c.Scale(0, 0, 0, 0)
	c.Translate(color.RGBA_01_64())
	dest.DrawImage(whiteImage, &ebi.DrawImageOptions{GeoM: g, ColorM: c})
}
