package helpers

import (
	"fmt"
	"strings"
)

var (
	Fmt     = fmt.Sprintf
	Sprintf = fmt.Sprintf
)

func StringEnclosedIn(s, pre, post string) string {
	start := strings.Index(s, pre)
	if start < 0 {
		return ""
	}
	start++
	s = s[start:]

	end := strings.Index(s, post)
	if end < 0 {
		return ""
	}
	s = s[:end]

	return s
}
