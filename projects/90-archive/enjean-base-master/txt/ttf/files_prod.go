// +build js production

package ttf

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ EMBED MODE ]
    Filesystem embedded into the executable itself for more
    compact production builds.
*/

import (
	df "g/files"

	"embed"
)

var Files = df.NewDirfiles(fsEmbed, []string{
	FantasqueSansMono_Bold_ttf,
	FantasqueSansMono_Bold_LargeLineHeight_ttf,
	FantasqueSansMono_Italic_ttf,
	FantasqueSansMono_Italic_LargeLineHeight_ttf,
	FantasqueSansMono_Regular_ttf,
	FantasqueSansMono_Regular_LargeLineHeight_ttf,
	Inconsolata_ExtraCondensed_Bold_ttf,
	Inconsolata_ExtraCondensed_Light_ttf,
	Inconsolata_ExtraCondensed_Regular_ttf,
})

// Filenames listed as constants for code autocompletion
const (
	FantasqueSansMono_Bold_ttf = "FantasqueSansMono-Bold.ttf"
	FantasqueSansMono_Bold_LargeLineHeight_ttf = "FantasqueSansMono-Bold_LargeLineHeight.ttf"
	FantasqueSansMono_Italic_ttf = "FantasqueSansMono-Italic.ttf"
	FantasqueSansMono_Italic_LargeLineHeight_ttf = "FantasqueSansMono-Italic_LargeLineHeight.ttf"
	FantasqueSansMono_Regular_ttf = "FantasqueSansMono-Regular.ttf"
	FantasqueSansMono_Regular_LargeLineHeight_ttf = "FantasqueSansMono-Regular_LargeLineHeight.ttf"
	Inconsolata_ExtraCondensed_Bold_ttf = "Inconsolata_ExtraCondensed-Bold.ttf"
	Inconsolata_ExtraCondensed_Light_ttf = "Inconsolata_ExtraCondensed-Light.ttf"
	Inconsolata_ExtraCondensed_Regular_ttf = "Inconsolata_ExtraCondensed-Regular.ttf"
)

  
//go:embed FantasqueSansMono-Bold.ttf
//go:embed FantasqueSansMono-Bold_LargeLineHeight.ttf
//go:embed FantasqueSansMono-Italic.ttf
//go:embed FantasqueSansMono-Italic_LargeLineHeight.ttf
//go:embed FantasqueSansMono-Regular.ttf
//go:embed FantasqueSansMono-Regular_LargeLineHeight.ttf
//go:embed Inconsolata_ExtraCondensed-Bold.ttf
//go:embed Inconsolata_ExtraCondensed-Light.ttf
//go:embed Inconsolata_ExtraCondensed-Regular.ttf
var fsEmbed embed.FS

