package txt

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/text"

	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
	"golang.org/x/image/font/sfnt"

	"image/color"

	ttf "g/txt/ttf"
	"io"
)

//
// Straight aliases
//

type Face = font.Face

var CacheGlyphs = text.CacheGlyphs

// TODO:  Write a benchmark to see at what point I actually need
//   to look at using the glyph cache...

// ---------------------------------------------------------------

type DbgArgs struct {
	DestImage *ebiten.Image
	Text      string
	X, Y      int
	// Color and Face are hard-set for Dbg text, it's always
	// a white on black shadow text of size 6x12,
	// some tiny bitmap font.
}

func Dbg(a DbgArgs) { ebitenutil.DebugPrintAt(a.DestImage, a.Text, a.X, a.Y) }

// ---------------------------------------------------------------

type DrawArgs struct {
	DestImage *ebiten.Image
	Text      string
	X, Y      int
	Color     color.Color
	Face      Face
}

func Draw(a DrawArgs) { text.Draw(a.DestImage, a.Text, a.Face, a.X, a.Y, a.Color) }

// NOTE:  Use `(font.Face).Metrics()` to get sizes of characters.
//   Look into `text.Draw()` for details into how to do spacing and such
//   with these characters for more fine-grained control.
// TODO:  Make a utility for using these metrics by string,
//   including how to make lines and such... We'll need to break lines
//   based on the width.

// ---------------------------------------------------------------

func GetFace(sizePt float64, fam *[]byte) Face {

	parsedFont := GetFont(fam)

	setting := faceSetting{size: sizePt, font: parsedFont}
	if cached, ok := faceCache[setting]; ok {
		return cached
	}

	// Standard default DPI for most screens. No need to detect it in a game,
	// it should always look the same.
	const DPI = 72

	face, err := opentype.NewFace(parsedFont, &opentype.FaceOptions{
		Size: sizePt, DPI: DPI, Hinting: font.HintingFull,
	})

	if err != nil {
		panic(err)
	}
	faceCache[setting] = face
	return face
}

func GetFont(fontDataRef *[]byte) *sfnt.Font {

	if cached, ok := fontParseCache[fontDataRef]; ok {
		return cached
	}

	parsedFont, err := opentype.Parse(*fontDataRef)

	if err != nil {
		panic(err)
	}
	fontParseCache[fontDataRef] = parsedFont
	return parsedFont
}

// ---------------------------------------------------------------

func GetFaceFile(filename string, size float64) Face {

	file, err := ttf.Files.FS.Open(filename)
	if err != nil {
		panic(err)
	}
	fontBytes, err := io.ReadAll(file)
	if err != nil {
		panic(err)
	}

	parsedFont, err := opentype.Parse(fontBytes)
	if err != nil {
		panic(err)
	}

	face, err := opentype.NewFace(parsedFont, &opentype.FaceOptions{
		Size:    size,
		DPI:     72,
		Hinting: font.HintingFull,
	})
	if err != nil {
		panic(err)
	}

	return face
}

// ---------------------------------------------------------------

var (
	fontParseCache = map[*[]byte]*sfnt.Font{}
	faceCache      = map[faceSetting]font.Face{}
)

type faceSetting struct {
	size float64
	font *sfnt.Font
}
