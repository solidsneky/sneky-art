package main

import (
	. "g/helpers"
	"g/klik"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

const (
	WIN_TITLE          = "FPS Meter"
	WIN_W              = 600
	WIN_H              = 480
	DYNAMIC_RESOLUTION = true // see Layout()
)

var (
	a = struct {
		quit           klik.Action
		lagSometimes   klik.Action
		causeHeavyLags klik.Action
	}{}
	bindings = []klik.KeyBind{
		{&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape},
		{&a.lagSometimes, ebi.KeyT},
		{&a.causeHeavyLags, ebi.KeyS},
	}
)

// ---------------------------------------------------------------

type g struct {
	meter FPSMeter
}

func (g *g) Update() error {

	// Necessary for the meter to work. Unless you want to use
	// your own thing!
	UpdateDt()
	// This would do the same thing, by the way.
	//    g.meter.ManualSnap(UpdateDt())
	g.meter.Snap()

	klik.UpdateControls(bindings, 0)
	if a.quit.IsJustPressed {
		return USER_EXIT
	}

	return nil
}
func (g *g) Draw(s *ebi.Image) {
	g.meter.Draw(s, 20, 20, keepFrameRecords, 100)
}

// ---------------------------------------------------------------

var keepFrameRecords = 200

func newGame() *g {
	g := &g{}
	g.meter.Init(keepFrameRecords)
	return g
}

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	Fmt("Starting: [%s]", WIN_TITLE)

	if err := ebi.RunGame(newGame()); err != nil {
		if err == USER_EXIT {
			return
		}
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	if DYNAMIC_RESOLUTION {
		// Resolution follows window size
		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}
