package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

const (
	WIN_TITLE = "Testing Text"
	WIN_W     = 360
	WIN_H     = 240
)

// ========================================

type g struct {
	keys []ebiten.Key
}

func (g g) Update() error {
	g.keys = inpututil.PressedKeys()
	return nil
}
func (g) Draw(s *ebiten.Image) {

	return
}

// ---------------------------------------------------------------

func main() {
	ebiten.SetWindowSize(WIN_W, WIN_H)
	ebiten.SetWindowTitle(WIN_TITLE)
	ebiten.SetWindowResizable(true)
	if err := ebiten.RunGame(g{}); err != nil {
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	return WIN_W, WIN_H
}
