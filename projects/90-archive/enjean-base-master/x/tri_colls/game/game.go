package game

import (
	"g/bang"
	. "g/helpers"
	"g/klik"
	"g/txt"
	"image"

	ebi "github.com/hajimehoshi/ebiten/v2"
	vec "github.com/hajimehoshi/ebiten/v2/vector"
)

const (
	WIN_TITLE = "Testing triangles"
	WIN_W     = 640
	WIN_H     = 480
)

var (
	a        = struct{ quit, diff klik.Action }{}
	bindings = []klik.KeyBind{{&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape}}
)

// ---------------------------------------------------------------

var (
	colChill     = Hsl(220, .6, .5)
	colBang      = Hsl(0, .5, .5)
	colContained = Hsla(120, .6, .8, .5)
	colContainer = Hsl(140, .6, .8)
	colActive    = Hsl(220, .5, .90)
)

var _emptyImg *ebi.Image = func() *ebi.Image {
	empty := ebi.NewImage(1, 1)
	sub := empty.SubImage(image.Rect(0, 0, 1, 1))
	empty.Fill(BasicColor.White)
	return sub.(*ebi.Image)
}()

func setColor(v *ebi.Vertex, c Color) {
	r, g, b, a := c.RGBA_01_32()
	v.ColorR, v.ColorG, v.ColorB, v.ColorA = r, g, b, a
}

type pt struct{ X, Y float64 }
type tri struct{ A, B, C pt }

// Conversions into the format for collisions -- straight arrays of data.
func (pt pt) Conv() [2]float64 {
	return [2]float64{pt.X, pt.Y}
}
func (tri tri) Conv() [3][2]float64 {
	return [3][2]float64{tri.A.Conv(), tri.B.Conv(), tri.C.Conv()}
}

// ---------------------------------------------------------------

type g struct {
	triangles       []tri
	createdVertices int

	collidingTriangles map[int]struct{}
}

func collectCollisions(tris []tri) (collIndexes map[int]struct{}) {
	collIndexes = map[int]struct{}{}
	for aidx, a := range tris {
		for bidx, b := range tris {
			if aidx == bidx {
				continue
			}
			if bang.TrianglesIntersect(a.Conv(), b.Conv()) {
				collIndexes[aidx] = struct{}{}
				collIndexes[bidx] = struct{}{}
			}
		}
	}
	return collIndexes
}

func (g *g) handleMouse() {
	m := klik.Mouse

	last := &g.triangles[len(g.triangles)-1]
	p := pt{X: float64(m.ScreenX), Y: float64(m.ScreenY)}
	switch g.createdVertices {
	case 0:
		last.A = p
	case 1:
		last.B = p
		last.C = pt{X: p.X + 1, Y: p.Y + 1}
	case 2:
		last.C = p
		// No need to sync, we'll create it right away
	}

	if m.ClickLeft.IsJustPressed {

		g.createdVertices++

		if g.createdVertices == 3 {
			g.createdVertices = 0
			g.triangles = append(g.triangles, tri{})
		}
	}
}
func (g *g) drawTris(s *ebi.Image) {

	var (
		colliding = []tri{}
		ok        = []tri{}
	)
	for idx, tri := range g.triangles {
		if _, collides := g.collidingTriangles[idx]; collides {
			colliding = append(colliding, tri)
		} else {
			ok = append(ok, tri)
		}
	}

	{
		path := &vec.Path{}
		for _, tri := range ok {
			a, b, c := tri.A, tri.B, tri.C
			path.MoveTo(float32(a.X), float32(a.Y))
			path.LineTo(float32(b.X), float32(b.Y))
			path.LineTo(float32(c.X), float32(c.Y))
		}
		drawPath(s, path, colChill)
	}
	{
		path := &vec.Path{}
		for _, tri := range colliding {
			a, b, c := tri.A, tri.B, tri.C
			path.MoveTo(float32(a.X), float32(a.Y))
			path.LineTo(float32(b.X), float32(b.Y))
			path.LineTo(float32(c.X), float32(c.Y))
		}
		drawPath(s, path, colBang)
	}
}

func drawPath(dest *ebi.Image, path *vec.Path, color Color) {
	verts, idxs := path.AppendVerticesAndIndicesForFilling(nil, nil)
	for i := range verts {
		setColor(&verts[i], color)
	}
	dest.DrawTriangles(
		verts, idxs,
		_emptyImg,
		&ebi.DrawTrianglesOptions{FillRule: ebi.FillAll})
}

func (g *g) drawDataDump(s *ebi.Image) {
	args := txt.DrawArgs{
		DestImage: s,
		Text:      "",
		X:         0,
		Y:         10,
		Color:     colContained,
		Face:      txt.GetFace(50, txt.MonoLight),
	}

	if true {
		args.Text = Doomp(g.triangles)
		txt.Draw(args)
	}

	if true {
		args.Text = Doomp(g.createdVertices)
		args.X = WIN_W / 2
		txt.Draw(args)
	}

	if true {
		args.Text = Doomp(g.collidingTriangles)
		args.X = WIN_W / 2
		args.Y = 20
		txt.Draw(args)
	}

	if false { // log mouse
		args.Text = Doomp(klik.Mouse)
		args.Y = WIN_H / 2
		args.Color = colBang
		txt.Draw(args)
	}
}

// ---------------------------------------------------------------

func (g *g) Update() error {
	klik.UpdateControls(bindings, 0)
	if a.quit.IsJustPressed {
		panic("end")
	}

	g.handleMouse()

	g.collidingTriangles = collectCollisions(g.triangles)

	return nil
}
func (g *g) Draw(s *ebi.Image) {

	g.drawDataDump(s)
	g.drawTris(s)

	return
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	return outsideW, outsideH
	//return WIN_W, WIN_H
}

// ---------------------------------------------------------------

func NewGame() *g {
	return &g{
		triangles:          []tri{{}}, // Has to have one initial triangle!
		createdVertices:    0,
		collidingTriangles: map[int]struct{}{},
	}
}

// ---------------------------------------------------------------
