package main

import (
	. "g/x/tri_colls/game"

	ebi "github.com/hajimehoshi/ebiten/v2"
)

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	if err := ebi.RunGame(NewGame()); err != nil {
		panic(err)
	}
}
