// +build !js,!production 

package main

/*
  GENERATED FILE. You probably shouldn't touch things here.

  [ RUNTIME MODE ]
    Filesystem mapped to the real files on disk. These files
    are expected to change.
*/

import (
	df "g/files"

	"os"
	. "g/helpers"
)

var fsReal = os.DirFS(ThisSourceFileDir())

var Files = df.NewDirfiles(fsReal, []string{
	To_reload_txt,
})

// Filenames listed as constants for code autocompletion
const (
    To_reload_txt = "to_reload.txt"
)
