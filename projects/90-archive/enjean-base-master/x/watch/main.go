package main

import (
	. "g/helpers"
	"g/klik"

	"g/files"
	"g/txt"

	ebi "github.com/hajimehoshi/ebiten/v2"
	"os"
)

const (
	WIN_TITLE          = "File Watching and Reloading"
	WIN_W              = 600
	WIN_H              = 300
	DYNAMIC_RESOLUTION = false // see Layout()
)

var (
	a        = struct{ quit klik.Action }{}
	bindings = []klik.KeyBind{{&a.quit, ebi.KeyQ}, {&a.quit, ebi.KeyEscape}}
)

// ---------------------------------------------------------------

type g struct{ text string }

func (g *g) Update() error {
	klik.UpdateControls(bindings, 0)
	if a.quit.IsJustPressed {
		os.Exit(0)
	}
	return nil
}
func (g *g) Draw(s *ebi.Image) {
	txt.Draw(txt.DrawArgs{
		Text: g.text, X: 10, Y: 10,
		Face: txt.GetFace(24, txt.MonoBold), Color: Hsl(340, .5, .5),
		DestImage: s,
	})
}

// ---------------------------------------------------------------

func newGame() *g {

	g := &g{
		text: "not reloaded yet",
	}

	//
	fn := func(f files.ChangedFile) { g.text = string(f.Bytes) }

	// The .Open call loads the file and runs the function immediatelly.
	// Everything in an .Open call is blocking, so you don't have to
	// signal out of the callback to parent.
	// If you want to, you can always use `go` to run the open call
	// in a goroutine of your own making.
	Files.Open(To_reload_txt, fn)

	// And the .Watch reaction runs it only when the file is changed,
	// it does not do anything right away.
	// Any calls made from .Watch are in a different goroutine,
	// the call to `fn` will be async.
	Files.Watch(To_reload_txt, fn)

	return g
}

func main() {
	ebi.SetWindowSize(WIN_W, WIN_H)
	ebi.SetWindowTitle(WIN_TITLE)
	ebi.SetWindowResizable(true)

	Fmt("Starting: [%s]\n", WIN_TITLE)

	if err := ebi.RunGame(newGame()); err != nil {
		panic(err)
	}
}
func (g) Layout(outsideW, outsideH int) (w int, h int) {
	if DYNAMIC_RESOLUTION {
		// Resolution follows window size
		return outsideW, outsideH
	} else {
		// Pixels get stretched when we resize
		return WIN_W, WIN_H
	}
}
